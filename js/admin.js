(function($) {

	$.fn.tagName = function() {
	   return $(this).prop('tagName').toLowerCase();
	}
	
	Drupal.imstyleman = {
			
		getParEl: function(self) {
			var idp = self.attr('id').split('-'), id;
			if (idp.length > 1) {
				idp.pop();
				id = idp.join('-');
			} else {
				id = '';
			}
			return $('#' +id);
		},
		
		closePrevCont: function() {
			var s = Drupal.settings.imstyleman;
			s.prevCont.addClass('hidden');
			var lg = s.prevCont.find('img.large-size');
			if (lg.length>0) {
				lg.remove();
				s.prevCont.addClass('hidden collapsed');
				setTimeout(function(){
					var s = Drupal.settings.imstyleman;
					s.prevCont.removeClass('collapsed');
				},250);
			}
		},
			
		init: function() {
			var s = Drupal.settings.imstyleman,i=0;
			s.cloneNames = $('.clone-name');
			
			s.cloneNames.parent().addClass('hidden');
			
			s.prevCont = $('#imstyleman-preview-container');
			
			$('input.update').on('change', function(e) {
				var it = $(this), chd = it.is(':checked'),el;
				el = Drupal.imstyleman.getParEl(it);
				if (el.length>0) {
					if (chd) {
						el.addClass('updatable');
					} else {
						el.removeClass('updatable');
						$('input.update-all').attr('checked',false);
					}
				}				
			});
			
			$('input.update-all').on('change', function(e) {
				var it = $(this), chd = it.is(':checked'), ups = $('input.update'), els = $('fieldset.style-data');
				if (els.length>0) {
					if (chd) {
						ups.attr('checked',true);
						els.addClass('updatable').removeClass('collapsed');
					} else {
						els.removeClass('updatable').addClass('collapsed');
						ups.attr('checked',false);
					}
				}				
			});
			
			$('input.delete').on('change', function(e) {
				var it = $(this), chd = it.is(':checked'),el;
				el = Drupal.imstyleman.getParEl(it);
				if (el.length>0) {
					el.removeClass('updatable');
					if (chd) {
						el.addClass('delible');
						el.find('textarea,input.form-text').attr('disabled','disabled');
					} else {
						el.removeClass('delible');
					}
				}				
			});
			
			$('select.clone-style').on('change', function(e) {
				var it = $(this), vl,el;
				vl = it.val();
				el = Drupal.imstyleman.getParEl(it);
				if (el.length>0) {
					if (/^\d/.test(vl)) {
						el.find('.clone-name').parent().removeClass('hidden');
					} else {
						el.find('.clone-name').parent().addClass('hidden');
					}
				}
			});
			
			$('.preview-image-wrapper .preview-image').on('click', function(e){
				e.stopImmediatePropagation();
				e.preventDefault();
				var it = $(this), im = it.find('img:first'), lg = im.clone(), s = Drupal.settings.imstyleman;
				if (s.prevCont.hasClass('collapsed') == false) {
					lg.removeAttr('style').addClass('large-size screen-size');
					var ww = $(window).width(), iw = im.attr('width'),
					title = 'Natural size: ' +  im.attr('width') + 'x' + im.attr('height') + 'px';
					if (iw > ww) {
						lg.addClass('enlargeable');
						title += '. Rescaled to fit screen. Click to view natural size!';
					} else {
						title += '. Click to hide!';
					}
					lg.attr('title', title)
					s.prevCont.append(lg);
					s.prevCont.removeClass('hidden');
				}
			});
			s.prevCont.on('click',function(e){
				e.stopImmediatePropagation();
				e.preventDefault();
				var lg = $(this).find('img:first');
				if (lg.hasClass('natural-size') == false && lg.hasClass('enlargeable')) {
					lg.removeClass('screen-size').addClass('natural-size');
					var tps = lg.attr('title').split('');
					lg.attr('title', tps.shift() + '. Click to hide!')
				} else {
					lg.removeClass('natural-size');
					Drupal.imstyleman.closePrevCont();
				}
			});
			
			$(document).on('keypress', function(e){
			    switch (e.which){
			    	case 0: case 27:
			    		Drupal.imstyleman.closePrevCont();
			    		break;
			    }
			});
		}
	};
	
  Drupal.behaviors.imstyleman = {
    attach : function(context) {
    	// now initialize
    	if (!Drupal.settings.imstyleman) {
    		Drupal.settings.imstyleman = {};
    	}
    	Drupal.imstyleman.init();
    }
  };
}(jQuery));
