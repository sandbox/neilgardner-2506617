<?php

function imstyleman_admin($form_state) {
	
	$form = array();
	module_load_include('admin.inc', 'image');
	$styles = image_styles();
	
	$form['styles'] = array(
			'#type' => 'fieldset',
			'#tree' => TRUE,
	);
	$link_opts = array('query' => array('destination' => $_GET['q']));
	$field_info = imstyleman_field_info($link_opts);
	$style_keys = array();
	$sizeInfo = array();
	foreach ($styles as $style_name => $style_data) {
		$key = "";
		$effects = array();
		$size = "";
		if (!empty($style_data['effects'])) {
			foreach ($style_data['effects'] as $effect) {
				$ed = $effect['data'];
				if (isset($ed['width']) || isset($ed['height'])) {
					$hasWidth = (is_numeric($ed['width']) && $ed['width'] > 0);
					$hasHeight = (is_numeric($ed['height']) && $ed['height'] > 0);
					if ($hasWidth || $hasHeight) {
						$width = $hasWidth? $ed['width'] : $ed['height'];
						$height = $hasHeight? $ed['height'] : $ed['width'];
						$area = $width * $height;
						$key = str_pad($area,10,"0", STR_PAD_LEFT);
						if (empty($ed['height'])) {
							$ed['height'] = 'auto';
						}
						$size = $ed['width']." x ".$ed['height'];
					}
				}
			}
		}
		if (empty($key)) {
			$key = str_repeat("9",10);
		}
		$key .= $style_name;
		$sizeInfo[$style_name] = $size;
		$style_keys[$key] = $style_name;
	}
	
	ksort($style_keys);
	
	$multiplier_options = array(
			'-' => 'none',
			'1/2' => '1/2 current size',
			'2/3' => '2/3 current size',
			'1.5' => '1.5x current size',
			'2' => '2x current size',
			'3' => '3x current size',
			'4' => '4x current size',
	);
	
	foreach ($style_keys as $key => $style_name) {
		$style_data = $styles[$style_name];
		$img_style_data = imstyleman_style_data($style_name);
		$label = preg_replace('#_+#',' ', $style_data['label']);
		if (!empty($sizeInfo[$style_name])) {
			$label .= " =&gt; " . $sizeInfo[$style_name];
		}
		$has_matched_fields = false;
		if (array_key_exists($style_name,$field_info)) {
			$has_matched_fields = (is_array($field_info[$style_name]) && !empty($field_info[$style_name]));
			if ($has_matched_fields) {
				$vars = array(
						'type' => 'ul',
						'title' => t("Used in"),
						'items' => $field_info[$style_name]
				);
			}
		}
		if ($has_matched_fields) {
			$num_fields = count($field_info[$style_name]);
			$label .= " (used in " . $num_fields . " display " . format_plural($num_fields,'setting','settings') . ")";  
		}
		
		$form['styles'][$style_name] = array(
				'#type' => 'fieldset',
				'#title' => $label,
				'#collapsible' => TRUE,
				'#collapsed' => TRUE,
				'#tree' => TRUE,
				'#attributes' => array('class' => array('style-data'))
		);
		$effects = array();
		if (!empty($style_data['effects'])) {
			foreach ($style_data['effects'] as $effect) {
				$effects[] = $effect['name'] . ": " . imstyleman_clean_data($effect['data']);
			}
		}
		$strEffects = implode("\n", $effects);
		$form['styles'][$style_name]['label'] = array(
				'#type' => 'textfield',
				'#title' => t("Display name"),
				'#default_value' => $style_data['label'],
				'#size' => 64,
		);
		
		$form['styles'][$style_name]['name'] = array(
				'#type' => 'textfield',
				'#title' => t("Machine name"),
				'#default_value' => $style_data['name'],
				'#size' => 64,
		);
		
		$edit_link = l('[edit]','admin/config/media/image-styles/edit/' . $style_name, $link_opts);
		$form['styles'][$style_name]['effects'] = array(
				'#type' => 'textarea',
				'#title' => t("Effects") . " " . $edit_link,
				'#default_value' => $strEffects,
				'#rows' => 3,
				'#cols' => 40,
		);
		
		if (!$has_matched_fields) {
			$form['styles'][$style_name]['delete'] = array(
					'#type' => 'checkbox',
					'#title' => t("Delete"),
					'#attributes' => array('title' => t("This image style is not used by any any image fields or picture groups"),  'class' => array('delete') )
			);
		}
		
		
		$form['styles'][$style_name]['update'] = array(
				'#type' => 'checkbox',
				'#title' => t("Update"),
				'#attributes' => array('title' => t("Changes will not be saved unless this option is checked"), 'class' => array('update'))
		);
		
		$form['styles'][$style_name]['multipliers'] = array(
				'#type' => 'select',
				'#title' => t("Clone style"),
				'#options' => $multiplier_options,
				'#attributes' => array('class' => array('clone-style') )
		);
		
		$form['styles'][$style_name]['clone_name'] = array(
				'#type' => 'textfield',
				'#title' => t("Cloned style name"),
				'#default_value' => $style_name . "_[size]",
				'#attributes' => array('class' => array('clone-name') )
		);
		$form['styles'][$style_name]['preview'] = array(
				'#type' => 'markup',
				'#prefix' => '<div class="preview-image-wrapper">',
				'#markup' => imstyleman_style_preview(array('style' => $style_data)),
				'#suffix' => '</div>',
		);
		
		if ($has_matched_fields) {
			$form['styles'][$style_name]['info'] = array(
					'#type' => 'markup',
					'#markup' => theme("item_list", $vars)
			);
			$form['styles'][$style_name]['#attributes']['class'][]  ='is-assigned';
		}
		
		$form['styles'][$style_name]['isid'] = array(
				'#type' => 'hidden',
				'#value' => $img_style_data->isid,
		);
		
		$form['styles'][$style_name]['ieids'] = array(
				'#type' => 'hidden',
				'#value' => $img_style_data->ieids,
		);
		
	}
	
	$form['styles']['update_all'] = array(
			'#type' => 'checkbox',
			'#title' => t("Update All"),
			'#description' => t("If selected, all image style settings will be processed and¨ updated from the above parameters. Please verify before saving"),
			'#attributes' => array('class' => array('update-all'))
	);
	
	$effects = imstyleman_fetch_effect_data();
	$arr_tokens = array();
	foreach ($effects as $effect_name => $effect_params) {
		$attrs = array();
		foreach ($effect_params as $key => $value) {
			$attrs[] = $key . ": " . $value;
		}
		$arr_tokens[] = $effect_name.': ' . implode(", ", $attrs);
	}
	
	$vars = array(
			'type' => 'ul',
			'title' => t("Valid effect names and attributes"),
			'items' => $arr_tokens
	);

	$form['tokens'] = array(
			'#type' => 'markup',
			'#markup' => theme('item_list', $vars)
	);
	
	$form['actions'] = array(
			'#type' => 'fieldset'
	);
	
	$form['actions']['save'] = array(
			'#type' => "submit",
			'#value' => t("Save"), 
	);
	$mod_path = drupal_get_path('module', 'imstyleman');
	$form['#attached'] = array(
			'css' => array(
					$mod_path . '/css/admin.css'
				),
			'js' => array(
					$mod_path . '/js/admin.js'
			),
	);
	$form['preview'] = array(
			'#type' => 'markup',
			'#markup' => '<div id="imstyleman-preview-container" class="hidden" title="click to hide"></div>'
	);
	$form['#validate'] = array('imstyleman_admin_validate');
	$form['#submit'] = array('imstyleman_admin_submit');
	return $form;
}

function imstyleman_fetch_effect_data($mode = 'simple') {
	$effects = image_effect_definitions();
	$effect_data = array();
	foreach ($effects as $effect_name => $effect) {
		$attrs = array();
		if (isset($effect['form callback']) && is_string($effect['form callback'])) {
			if (function_exists($effect['form callback'])) {
				$info = call_user_func($effect['form callback'], $effect['data']);
				if (is_array($info) && !empty($info)) {
					foreach ($info as $key => $value) {
						if (strpos($key,'#') !== 0) {
							$ed = array(
									'type' => "string",
									'unit' => NULL,
									'default' => NULL,
									'validate' => NULL
							);
							if (isset($value['#default_value'])) {
								$ed['default'] = $value['#default_value'];
							}
							if (isset($value['#element_validate'])) {
								$ed['validate'] = $value['#element_validate'];
							}
							$ed['element'] = $value;
							switch ($value['#type']) {
								case 'textfield':
									if (isset($value['#field_suffix'])) {
										$ed['unit'] = trim($value['#field_suffix']);
										switch ($ed['unit']) {
											case 'pixels':
											case '%':
											case '&deg;':
												$ed['type'] = 'int';
												break;
										}
									}
									break;
								case 'checkbox':
									$ed['type'] = "boolean";
									break;
							}
							if ($mode == 'map') {
								$attrs[$key] = $ed;
							}
							else {
								$info = $ed['type'];
								if (!empty($ed['unit'])) {
									$info .= ' ' . $ed['unit'];
								}
								$attrs[$key] = $info;
							}
					
						}
					}
				}
			}
		}
		$effect_data[$effect_name] = $attrs;
	}
	return $effect_data;
}

function imstyleman_admin_validate(&$form, &$form_state) {
	_imstyleman_admin_process($form, $form_state,'validate');
}

function _imstyleman_admin_process(&$form, &$form_state,$mode = 'validate') {
	$updated = false;
	$values = $form_state['values'];
	$effect_data = imstyleman_fetch_effect_data('map');
	$update_all = (bool) $values['styles']['update_all'];
	if (isset($values['styles'])) {
		foreach ($values['styles'] as $style => $style_data) {
			if (is_array($style_data)) {
				$update = ($update_all || (isset($style_data) && $style_data['update'] > 0));
				if ($update && isset($style_data['effects']) && is_string($style_data['effects'])) {
					$updated_style = imstyleman_admin_validate_style($style, $style_data,$form,$form_state, $effect_data,$mode);
					if ($updated_style && !$updated) {
						$updated = true;
					}
				}
				if ($mode == 'submit') {
					_imstyleman_manage_styleman($style,$style_data);
				}
			}
		}
	}
	return $updated;
}

function _imstyleman_manage_styleman($style = NULL, array &$style_data) {
	if (isset($style_data['delete']) && $style_data['delete'] > 0) {
		if ($style_data['delete'] == 1) {
			$source_style = image_style_load($style,$style_data['isid']);
			image_style_delete($source_style);
		}
	}

	if (isset($style_data['multipliers'])) {
		if (!empty($style_data['multipliers']) && $style_data['multipliers'] != '-' && preg_match('#^\d#',$style_data['multipliers'])) {
			list($multiplier,$divisor) = explode('/',$style_data['multipliers']);
			if (is_numeric($multiplier)) {
				$multiplier = (float) $multiplier;
				if (!empty($divisor) && is_numeric($divisor)) {
					$divisor = (int) $divisor;
					$multiplier = $multiplier / $divisor;
				}
				$source_style = image_style_load($style,$style_data['isid']);
				$target_style = $source_style;
				if (!empty($target_style['effects']) && is_array($target_style['effects'])) {
					unset($target_style['isid']);
					$add_appendix = false;
					$has_clone_name = false;
					$dims = array('width', 'height');
					if (isset($style_data['clone_name']) && strlen($style_data['clone_name']) > 4) {
						$style_data['clone_name'] = trim($style_data['clone_name']);
						if (preg_match('#^(.*?)_?(\[size\])$#',$style_data['clone_name'],$match)) {
							$style_data['clone_name'] = $match[1];
							$add_appendix = true;
						}
						$target_style['name'] = trim($style_data['clone_name']);
						if (strlen($style_data['clone_name']) > 4) {
							$has_clone_name = true;
						}
					}
					if (!$has_clone_name) {
						$target_style['name'] = $style;
						$add_appendix = true;
					}
					if ($add_appendix) {
						$appendix = $multiplier > 1? $multiplier.'x' : $style_data['multipliers'];
						$appendix_regex = '#[_ ]((\d+)(x|[_ ]\d+))$#';
						if (preg_match($appendix_regex, $style, $match)) {
							$current_multiplier = (float) $match[2];
							$current_divisor = 1;
							if ($match[3] != 'x') {
								$current_divisor = (int) str_replace('_','', $match[3]);
							}
							$new_multiplier = ($current_multiplier * $multiplier);
							if ($new_multiplier > 0) {
								$appendix = $new_multiplier.'x';
							}
							$target_style['name'] = preg_replace($appendix_regex,'',$target_style['name']);
							$target_style['label'] = preg_replace($appendix_regex,'',$target_style['label']);
						}
						$target_style['name'] .= "_" . str_replace('/','_',$appendix);
						$target_style['label'] .= " " . $appendix;
					}
					$target_style['name'] = preg_replace('#[^a-z0-9_-]+#', '_', $target_style['name']);
					$target_style['label'] = preg_replace('#_+#', ' ', $target_style['label']);
					$target_style = image_style_save($target_style);
					$new_effects = array();
					$weight = 0;
					foreach ($source_style['effects'] as $index => $effect) {
						$ed = $effect['data'];
						foreach ($dims as $dim) {
							if (isset($ed[$dim]) && is_numeric($ed[$dim]) && $ed[$dim] > 0) {
								$ed[$dim] *= $multiplier;
							}
						}
						$weight++;
						$effect['isid'] = $target_style['isid'];
						$effect['weight'] = $weight;
						$effect['data'] = $ed;
						$effect['ieid'] = NULL;
						image_effect_save($effect);
					}
				}
			}
		}
	}
}

function _imstyleman_parse_existing_effects(array $style_data) {
	$existing_effects = array();
	if (!empty($style_data['ieids'])) {
		$matched_effects = explode(',', $style_data['ieids']);
		$existing_effects = array();
		if (!empty($matched_effects)) {
			foreach ($matched_effects as $matched_effect) {
				list($matched_effect_name,$ieid,$matched_weight) = explode(':', $matched_effect);
				if (is_string($matched_effect_name) && is_numeric($ieid) && $ieid > 0) {
					$existing_effects[] = array(
							"name" => trim($matched_effect_name),
							"ieid" => (int) $ieid,
							"weight" => (int) $matched_weight,
					);
				}
			}
		}
	}
	return $existing_effects;
}

function _imstyleman_remove_unmatched_effects(array $existing_effects,array $saved_ieds, $isid = 0) {
	$unmatched_ieids = array();
	foreach ($existing_effects as $effect_hash) {
		if (!in_array($effect_hash['ieid'],$saved_ieds)) {
			$unmatched_ieids[] = $effect_hash['ieid'];
		}
	}
	if (!empty($unmatched_ieids) && $isid > 0) {
		$query = db_delete('image_effects')->condition('isid',$isid)->condition('ieid',$unmatched_ieids,"IN");
		$query->execute();
	}
}

function imstyleman_admin_validate_style($style, &$style_data,&$form,&$form_state, &$effect_data,$mode = 'validate') {
	$updated = false;
	$saved_ieds = array();
	$effect_lines = $parts = explode("\n", $style_data['effects']);
	$parents = array('styles', $style, 'effects');
	if ($mode == 'submit') {
		$existing_effects = _imstyleman_parse_existing_effects($style_data);
	}
	$weight = 0;
	$isid = $style_data['isid'];
	if ($isid > 0 && !empty($effect_lines)) {
		
		$source_style = image_style_load($style,$style_data['isid']);
		if (is_array($source_style)) {
			$source_style['name'] = trim($style_data['name']);
			$source_style['label'] = trim(str_replace('_',' ', $style_data['label']));
			image_style_save($source_style);
		}
		
		foreach ($effect_lines as $effect_string) {
			if (is_string($effect_string)) {
				if (preg_match('#^\w+\s*:?#',$effect_string)) {
					$effect_struct = imstyleman_effect_process($style, $effect_string, $style_data, $form, $form_state, $effect_data,$parents);
					$ieid = 0;
					if ($mode == 'submit' && $effect_struct->valid) {
						if (!empty($existing_effects)) {
							foreach ($existing_effects as $effect_hash) {
								if ($effect_hash['name'] == $effect_struct->name) {
									$ieid = $effect_hash['ieid'];
									break;
								}
							}
						}
						$weight++;
						imstyleman_effect_save($isid, $weight, $effect_struct->name,$effect_struct->attributes,$ieid);
						$updated = true;
						if ($ieid > 0) {
							$saved_ieds[] = $ieid;
						}
					}
				}
			}
		}
	}
	if ($mode == 'submit' && !empty($existing_effects)) {
		_imstyleman_remove_unmatched_effects($existing_effects,$saved_ieds, $isid);
	}
	return $updated;
}

function imstyleman_effect_process($style, $effect_string, &$style_data,&$form,&$form_state, &$effect_data,&$parents) {
	$effect_struct = new StdClass;
	$effect_struct->valid = false;
	$parts = explode(':', trim($effect_string));
	$num_parts = count($parts);
	$attributes = array();
	$element = array('#parents' => $parents);
	if ($num_parts > 0) {
		$effect_struct->name = array_shift($parts);
		$effect_struct->name = trim($effect_struct->name);
		if (array_key_exists($effect_struct->name,$effect_data)) {
			$effect_struct->valid = true;
			if ($num_parts > 1) {
				$remainder = implode(':', $parts);
				$remainder = trim($remainder);
					
				$attr_strs = explode(',', $remainder);
				foreach ($attr_strs as $str) {
					$parts = preg_split('#\s*[:=]\s*#', $str);
					if (count($parts)> 1) {
						$attr_name = preg_replace('#[^a-z0-9_-]#i','',$parts[0]);
						$attr_val = preg_replace('#[^a-z0-9_-]#i','',$parts[1]);
						$attributes[$attr_name] = trim($attr_val);
					}
				}
				$effect_attrs = $effect_data[$effect_struct->name];
				
				foreach ($effect_attrs as $attr => $info) {
					if (!isset($attributes[$attr])) {
						$attributes[$attr] = $info['default'];
					}
					$funcs = $info['validate'];
					if (!empty($funcs) && is_array($funcs)) {
						foreach ($funcs as $func) {
							if (function_exists($func)) {
								$element += $info['element'];
								$element['#value'] = $attributes[$attr];
								$func($element,$form_state);
							}
						}
					}
					else {
						switch ($info['type']) {
							case 'boolean':
								if (is_string($attributes[$attr])) {
									$attributes[$attr] = strtolower($attributes[$attr]);
									switch ($attributes[$attr]) {
										case 'true':
										case 'yes':
											$attributes[$attr] = 1;
											break;
										case 'false':
										case 'no':
											$attributes[$attr] = 0;
											break;
									}
								}
								if (!is_numeric($attributes[$attr])) {
									form_error($element, t("The @attribute attribute must be set to 0 or 1", array("@attribute" => $attr)));
									$effect_struct->valid = false;
								} else {
									$attributes[$attr] = (int) $attributes[$attr];
									if ($attributes[$attr] > 1) {
										$attributes[$attr] = 1;
									}
									else if ($attributes[$attr] < 0) {
										$attributes[$attr] = 0;
									}
								}
								break;
						}
					}
				}
			}
		}
		else {
			form_error($element, t("Effect @name is not available", array("@name" => $effect_struct->name)));
		}
	}
	$effect_struct->attributes = $attributes;
	return $effect_struct;
}

function imstyleman_admin_submit(&$form, &$form_state) {
	$updated = _imstyleman_admin_process($form, $form_state,'submit');
	if ($updated) {
		imstyleman_clear_cache();
	}
}

function imstyleman_clean_data($data) {
	$strData = "";
	if (is_array($data)) {
		$strData = json_encode($data);
		$strData = preg_replace('#["{}\[\]\']#','',$strData);
		$strData = preg_replace('#,#',', ',$strData);
	}
	return $strData;
}


function imstyleman_field_info(array $link_opts) {
	$image_styles = array();
	$match_setting_types = array("image_style","picture_group","colorbox");
	$query = db_select("field_config_instance","fci")->fields("fci", array('field_name','entity_type','bundle',"data"));
	$query->join("field_config","fc","fci.field_id=fc.id");
	$query->fields("fc", array("type","module"));
	$query->condition("fc.type",array("image","file"), "IN");
	$query->condition("fci.data",'image_style";s:[1-9]', "REGEXP");
	$result = $query->execute();
	if ($result) {
		$data = $result->fetchAll();
		if (!empty($data)) {
			foreach ($data as $index => $row) {
				if (!empty($row->data)) {
					$row->data = unserialize($row->data);
					if (!empty($row->data['display']) && is_array($row->data['display'])) {
						foreach ($row->data['display'] as $view_mode => $display) {
							if (isset($display['settings'])) {
								foreach ($match_setting_types as $type) {
									if (isset($display['settings'][$type])) {
										$style_name = $display['settings'][$type];
										$style_refs = array();
										switch ($type) {
											case 'picture_group':
												$pg = picture_mapping_load($style_name);
												if (is_object($pg)) {
													if (isset($pg->mapping) && is_array($pg->mapping)) {
														$index = 0;
														foreach ($pg->mapping as $k => $md) {
															if (is_array($md)) {
																$ps = explode('.',$k);
																$bp = array_pop($ps);
																foreach ($md as $multiplier => $mv) {
																	$style_refs[ $bp . '_' . $multiplier] = $mv;
																	$index++;
																}
															}
														}
													}
												}
												break;
											default:
												$style_refs = array('all' => $style_name);
												break;
										}
										foreach ($style_refs as $context => $style_val) {
											if (empty($style_val)) {
												$style_val = 'original';
											}
											if (!array_key_exists($style_val,$image_styles)) {
												$image_styles[$style_val] = array();
											}
											
											$context_info = str_replace('_',' ', $context);
											$typeName = str_replace('_',' ', $type);
											$path = '';
											$bundle_ref = str_replace('_','-', $row->bundle);
											switch ($row->entity_type) {
												case 'node':
													$path = '/admin/structure/types/manage/'.$bundle_ref.'/display/' . $view_mode;
													break;
												case 'field_collection_item':
													$path = '/admin/structure/field-collections/'. $bundle_ref . '/display/' . $view_mode;
													break;
											}
											if (!empty($path)) {
												$view_path = l($view_mode,$path,$link_opts);
											}
											else {
												$view_path = $view_mode;
											}
											$image_styles[$style_val][] = $view_path . ' ('.$typeName.': '.$context_info.')' ." =&gt; " . $row->entity_type . ":" . $row->bundle . ":" . $row->field_name;
										}
									}
								}
							}
						}
					}
				}
				
			}
		}
	}
	return $image_styles;
}

function imstyleman_style_data($style_name) {
	$image_styles = array();
	$query = db_select("image_effects","eff")->fields("eff", array('ieid',"isid","name","weight") );
	$query->leftJoin("image_styles","ims","eff.isid=ims.isid");
	$query->condition("ims.name",$style_name);
	$query->orderBy("eff.weight");
	$query->orderBy("eff.ieid");
	$result = $query->execute();
	$data = new StdClass;
	$data->isid = 0;
	$data->effects = array();
	$data->ieids = array();
	$data->num_effects = 0;
	if ($result) {
		$rows = $result->fetchAll();
		if (!empty($rows)) {
			foreach ($rows as $index => $row) {
				if ($index == 0) {
					$data->isid = $row->isid;
				}
				$data->effects[] = $row;
				$data->ieids[] = $row->name . ":" . $row->ieid . ":" . $row->weight;
			}
		}
		else {
			$query = db_select("image_styles","ims")->fields("ims", array("isid") );
			$query->condition('name', $style_name);
			$result = $query->execute();
			if ($result) {
				$data->isid = $result->fetchField();
			}
		}
		if (!empty($data->effects)) {
			$data->num_effects = count($data->effects);
			$data->ieids = implode(',', $data->ieids);
		}
		else {
			$data->ieids = '';
		}
	}
	return $data;
}

function imstyleman_effect_save($isid, $weight =1, $effect_name = NULL, $data, $ieid = 0) {
	$result = 0;
	if (is_numeric($isid) && $isid > 0 && is_string($effect_name) && strlen($effect_name) > 2 && is_array($data)) {
		$hash = array(
				'isid' => $isid,
				'weight' => $weight,
				'name' => $effect_name,
				'data' => serialize($data),
		);
		$update = (is_numeric($ieid) && $ieid > 0);
		$table = 'image_effects';
		if ($update) {
			$func = 'db_update';
		}
		else {
			$func = 'db_insert';
		}
		$query = $func($table)->fields($hash);
		if ($update) {
			$ieid = (int) $ieid;
			$query->condition('ieid', $ieid);
		}
		$result = (int) $query->execute();
	}
	return $result > 0;
}

function imstyleman_clear_cache() {
	cache_clear_all('image_styles', 'cache');
	cache_clear_all('image_effects:', 'cache', TRUE);
	drupal_static_reset('image_styles');
	drupal_static_reset('image_effects');
	// Clear field caches so that formatters may be added for this style.
	field_info_cache_clear();
}

/*
 * Theme
 */
	
/**
 * Returns HTML for a preview of an image style.
 *
 * @param $variables
 *   An associative array containing:
 *   - style: The image style array being previewed.
 *
 * @ingroup themeable
 */
function imstyleman_style_preview($variables) {
	$style = $variables['style'];

	$sample_image = variable_get('image_style_preview_image', drupal_get_path('module', 'imstyleman') . '/images/high-definition-apples.jpg');
	$sample_width = 320;
	$sample_height = 320;
	$divisor = 8;
	$link_opts = array('attributes' => array('target' => '_blank'));
	// Set up original file information.
	$original_path = $sample_image;
	$original_image = image_get_info($original_path);
	if ($original_image['width'] > $original_image['height']) {
		$original_width = min(($original_image['width']/$divisor), $sample_width);
		$original_height = round($original_width / $original_image['width'] * $original_image['height']);
	}
	else {
		$original_height = min(($original_image['height']/$divisor), $sample_height);
		$original_width = round($original_height / $original_image['height'] * $original_image['width']);
	}
	$original_attributes = array_intersect_key($original_image, array('width' => '', 'height' => ''));
	$original_attributes['style'] = 'width: ' . $original_width . 'px; height: ' . $original_height . 'px;';

	// Set up preview file information.
	$preview_file = image_style_path($style['name'], $original_path);
	if (!file_exists($preview_file)) {
		image_style_create_derivative($style, $original_path, $preview_file);
	}
	$preview_image = image_get_info($preview_file);
	if ($preview_image['width'] > $preview_image['height']) {
		$preview_width = min( ($preview_image['width'] / $divisor), $sample_width);
		$preview_height = round($preview_width / $preview_image['width'] * $preview_image['height']);
	}
	else {
		$preview_height = min( ($preview_image['height']/ $divisor ), $sample_height);
		$preview_width = round($preview_height / $preview_image['height'] * $preview_image['width']);
	}
	$preview_attributes = array_intersect_key($preview_image, array('width' => '', 'height' => ''));
	$preview_attributes['style'] = 'width: ' . $preview_width . 'px; height: ' . $preview_height . 'px;';

	// In the previews, timestamps are added to prevent caching of images.
	$output = '<section class="image-style-preview preview clearfix">';

	// Build the preview of the original image.
	$original_url = file_create_url($original_path);
	$output .= '<div class="preview-image-wrapper">';
	$output .= '<figure class="preview-image original-image" style="' . $original_attributes['style'] . '" title="'.t('view actual size').'">';
	$output .= theme('image', array('path' => $original_path, 'alt' => t('Sample original image'), 'title' => '', 'attributes' => $original_attributes));
	$output .= '<div class="height" style="height: ' . $original_height . 'px"><span>' . $original_image['height'] . 'px</span></div>';
	$output .= '<div class="width" style="width: ' . $original_width . 'px"><span>' . $original_image['width'] . 'px</span></div>';
	$output .= '</figure>'; // End preview-image.
	$output .= '</div>'; // End preview-image-wrapper.

	// Build the preview of the image style.
	$preview_url = file_create_url($preview_file) . '?cache_bypass=' . REQUEST_TIME;
	$output .= '<div class="preview-image-wrapper">';
	$output .= '<figure class="preview-image modified-image" style="' . $preview_attributes['style'] . '" title="'.t('view actual size').'">';
	$output .= theme('image', array('path' => $preview_url, 'alt' => t('Sample modified image'), 'title' => '', 'attributes' => $preview_attributes));
	$output .= '<div class="height" style="height: ' . $preview_height . 'px"><span>' . $preview_image['height'] . 'px</span></div>';
	$output .= '<div class="width" style="width: ' . $preview_width . 'px"><span>' . $preview_image['width'] . 'px</span></div>';
	$output .= '</figure>'; // End preview-image.
	$output .= '</div>'; // End preview-image-wrapper.

	$output .= '</section>'; // End image-style-preview.

	return $output;
}